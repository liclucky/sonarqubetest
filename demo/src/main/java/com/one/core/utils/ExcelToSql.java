package com.one.core.utils;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class ExcelToSql {
    public static void main(String[] args) {
        String excelFilePath = "/Users/yiwudong/Desktop/tbl_demo.xlsx";
        String tableName = "your_table_name";//表名称

        try {
            Workbook workbook = new XSSFWorkbook(new File(excelFilePath));
            Sheet sheet = workbook.getSheetAt(0);
            // 读取表头，建立列名和索引的映射
            Row headerRow = sheet.getRow(0);
            Map<String, Integer> columnMap = new HashMap<>();
            for (int i = 0; i < headerRow.getLastCellNum(); i++) {
                Cell cell = headerRow.getCell(i);
                if (cell!= null) {
                    columnMap.put(cell.getStringCellValue(), i);
                }
            }
            // 构建插入SQL语句
            List<String> sqlList = new ArrayList<>();
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                Row dataRow = sheet.getRow(i);
                //StringBuilder sqlBuilder = new StringBuilder("INSERT INTO " + tableName + " (id, ");
                StringBuilder sqlBuilder = new StringBuilder("INSERT INTO " + tableName + " (id, ");
                StringBuilder valueBuilder = new StringBuilder(" VALUES (");
                UUID uuid = UUID.randomUUID();
                valueBuilder.append(uuid.toString()).append("', ");

                boolean first = true;
                for (Map.Entry<String, Integer> entry : columnMap.entrySet()) {
                    if (!first) {
                        sqlBuilder.append(", ");
                        valueBuilder.append(", ");
                    }
                    sqlBuilder.append(entry.getKey());
                    Cell cell = dataRow.getCell(entry.getValue());
                    if (cell == null) {
                        valueBuilder.append("NULL");
                    } else if (cell.getCellType() ==  CellType.STRING) {
                        valueBuilder.append("'").append(cell.getStringCellValue()).append("'");
                    } else if (cell.getCellType() == CellType.NUMERIC) {
                        valueBuilder.append(cell.getNumericCellValue());
                    } else {
                        valueBuilder.append("NULL");
                    }
                    first = false;
                }
                sqlBuilder.append(")");
                valueBuilder.append(");");
                sqlList.add(sqlBuilder.toString() + valueBuilder.toString());
            }
            // 将SQL语句写入到txt文件
            FileOutputStream fos = new FileOutputStream("output.sql");
            for (String sql : sqlList) {
                fos.write(sql.getBytes());
                fos.write("\n".getBytes());
            }
            fos.close();
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            throw new RuntimeException(e);
        }
    }
}

