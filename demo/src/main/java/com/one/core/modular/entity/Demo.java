package com.one.core.modular.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.util.Date;

@Data
@Accessors(chain = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "tbl_demo")
public class Demo {

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 姓名
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 手机号
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 创建人id
     */
    @TableField(value = "create_id")
    private String createId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新人id
     */
    @TableField(value = "update_id")
    private String updateId;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 逻辑删除 0-否 1-是
     */
    @TableField(value = "is_del")
    @TableLogic
    private Integer isDel;

    public void setId(String id) {
        this.id = id;
        if ("01".equals(id)){
            this.name="张三";
            this.phone="110";
        }
    }

}

