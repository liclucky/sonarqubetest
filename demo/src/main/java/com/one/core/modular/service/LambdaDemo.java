package com.one.core.modular.service;

public class LambdaDemo {
    public static void main(String[] args) {
        // 使用Lambda表达式实现MyFunction接口
        MyFunction myLambda = (x, y) -> System.out.println("Result: " + (x + y));

        // 调用Lambda表达式
        myLambda.apply(10, 20);
    }
}
