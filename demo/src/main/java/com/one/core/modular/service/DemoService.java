package com.one.core.modular.service;

import com.one.core.modular.dao.DemoMapper;
import com.one.core.modular.entity.Demo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DemoService {
    private final DemoMapper demoMapper;

    public Demo getDemoById(String id) {
        return demoMapper.selectById(id);
    }


    public List<Demo> getAll() {
        return demoMapper.selectList(null);
    }


}
