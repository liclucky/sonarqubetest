package com.one.core.modular.controller;

import com.one.core.modular.entity.Demo;
import com.one.core.modular.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/java")
    public String test(){
        Runnable runnable = () -> System.out.println("lamdba表达式");
        runnable.run();
        List<String> list = Arrays.asList("b", "f", "c","d","e","a");
        ArrayList<String> arrayList = new ArrayList<>(list);
        System.out.println(arrayList);
        arrayList.sort((s1, s2) -> s1.compareTo(s2));
        System.out.println(arrayList);
        //System.out.println(lamdba表达式.toString());
        System.out.println("hello java");
        return "hello java2"+list;
    }

    /**
     * 读取本地文件
     */
    @RequestMapping("/filetest")
    public  void filetest() {

        File file = new File("/Users/yiwudong/Desktop/project_springboot/demo/src/main/java/com/one/core/modular/controller/rule.xml");  // 文件路径和名称
//        try {
//            FileInputStream fis = new FileInputStream(file);
//
//            int data;
//            while ((data = fis.read()) != -1) {
//                System.out.print((char) data);
//            }
//            System.out.println("\"测试11==\"");
//            fis.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        try {
            FileReader fr = new FileReader(file);

            int data;
            while ((data = fr.read()) != -1) {
                System.out.print((char) data);
            }
            System.out.println("\"测试22==\"");
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private DemoService demoService;

    /**
     * 通过id查询数据
     * @param id
     * @return
     */
    @RequestMapping("/getDemoById")
    public Demo getDemoById(String id) {
        System.out.println("获取的参数="+id);
        demoService.getDemoById(id);
            return demoService.getDemoById(id);
    }

    @RequestMapping("/getAll")
    public List<Demo> getDemoList(){

        return  demoService.getAll();
    }
}
