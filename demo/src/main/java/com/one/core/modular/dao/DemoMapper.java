package com.one.core.modular.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.one.core.modular.entity.Demo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DemoMapper extends BaseMapper<Demo> {
}
