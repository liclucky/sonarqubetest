package com.one.core.modular.service;

@FunctionalInterface
public interface MyFunction {
    void apply(int x,int y);
}

//class LambdaDemo2{
//    public static void main(String[] args) {
//        MyFunction myFunction=(x,y) -> System.out.println("Result: " + (x + y) );
//        //MyFunction myLambda = (x, y) -> System.out.println("Result: " + (x + y));
//        myFunction.apply(1,2);
//    }
//}



