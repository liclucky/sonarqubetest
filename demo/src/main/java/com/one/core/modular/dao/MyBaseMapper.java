package com.one.core.modular.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;

public interface MyBaseMapper<T> extends BaseMapper<T> {
    //根据ID逻辑删除一条记录，传入参数是一个实体对象，id不能为空
    int deleteByIdWithFill(T entity);
    //根据Wrapper条件构造器构造条件进行批量删除
    int deleteBatchWithFill(@Param(Constants.ENTITY) T entity, @Param(Constants.WRAPPER) Wrapper<T> wrapper);
    //批量插入数据
    Integer insertBatchSomeColumn(Collection<T> entityList);
}
