package com.one.core;

import com.one.core.modular.dao.DemoMapper;
import com.one.core.modular.entity.Demo;
import com.one.core.modular.service.DemoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Date;
import java.util.List;

@SpringBootTest
class CoreApplicationTests {

    @Resource
    private DemoService demoService;

    @Autowired
    private DemoMapper demoMapper;

    @Test
    void contextLoads() {
//        Demo demo = new Demo();
//        demo.setId("02");
//
//        System.out.println("id: " + demo.getId());
//        System.out.println("name: " + demo.getName());
//        System.out.println("phone: " + demo.getPhone());


//        Demo demo1 = demoService.getDemoById("1");
//        System.out.println(demo1);
//        System.out.println("11111111");
//        Demo demo = demoService.getDemoById("1");
//        System.out.println("!!!!!!!!!!!!!!!!!");
//        System.out.println(demo);


        Demo build = Demo.builder().name("ceshi").phone("111111111111").createId("ywd").createTime(new Date()).updateTime(new Date()).build();
        int insert = demoMapper.insert(build);
        System.out.println("影响行数："+insert);

        List<Demo> demos = demoMapper.selectList(null);
        demos.forEach(System.out::println);

//        List<String> data = new ArrayList<>();
//        try {
//            for (int i = 0; i < 1000000; i++) {
//                data.add(new String(new byte[1024 * 1024]));
//            }
//        } catch (OutOfMemoryError e) {
//            System.out.println("Out of memory error: Java Heap Space");
//        }

        /*Socket socket = null;
        String xml="<datamatch column=\"cusyname\" value=\"客户姓名\">[\\u4e00-\\u9fa5\\w\\s][1,25]</datamatch>\n" +
                "    <datamatch column=\"certcode\" value=\"证件编号\">[\\s\\s][1,2]</datamatch>";
        try {
            socket = new Socket("127.0.0.1",9999);
            OutputStream outputStream = socket.getOutputStream();
            byte[] b = xml.getBytes("utf-8");
            outputStream.write(b);
            InputStream inputStream = socket.getInputStream();
            byte[] bytes = new byte[2048];
            int len;
            len = inputStream.read(bytes);
            System.out.println(new String(bytes,0,len));
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }*/
    }



    
}
